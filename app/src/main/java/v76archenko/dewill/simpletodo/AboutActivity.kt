package v76archenko.dewill.simpletodo

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.Toast
import com.github.clans.fab.FloatingActionButton


class AboutActivity : AppCompatActivity() {
  private var gmail: FloatingActionButton? = null
  private var bitbucket: FloatingActionButton? = null
  private var github: FloatingActionButton? = null
  private var linkedin: FloatingActionButton? = null
  private val URL_LINKEDIN = "https://goo.gl/apCXbe"
  private val URL_BITBUCKET = "https://goo.gl/znBkSK"
  private val URL_GITHUB = "https://goo.gl/37D6c8"

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_about)
    val toolbar = findViewById<Toolbar>(R.id.about_toolbar)
    setSupportActionBar(toolbar)
    initFloatingActionButton()

    gmail!!.setOnClickListener { _ -> sendEmail() }

    bitbucket!!.setOnClickListener({ _ -> openUrl(URL_BITBUCKET) })

    github!!.setOnClickListener({ _ -> openUrl(URL_GITHUB) })

    linkedin!!.setOnClickListener({ _ -> openUrl(URL_LINKEDIN) })
  }

  private fun initFloatingActionButton() {
    gmail = findViewById(R.id.about_fab_email)
    bitbucket = findViewById(R.id.about_fab_bitbucket)
    github = findViewById(R.id.about_fab_github)
    linkedin = findViewById(R.id.about_fab_linkedin)
  }

  private fun sendEmail() {
    Log.i("Send email", "")
    val TO = arrayOf("v77archenko@gmail.com")
    val SUBJECT = arrayOf(resources.getString(R.string.app_name))
    val emailIntent = Intent(Intent.ACTION_SEND)

    emailIntent.data = Uri.parse("mailto:v77archenko@gmail.com")
    emailIntent.type = "text/plain"
    emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
    emailIntent.putExtra(Intent.EXTRA_SUBJECT, SUBJECT)

    try {
      startActivity(Intent.createChooser(emailIntent, "Send mail to the author"))
      finish()
    } catch (ex: android.content.ActivityNotFoundException) {
      Toast.makeText(this@AboutActivity, "There is no email client installed.",
          Toast.LENGTH_SHORT).show()
    }

  }

  private fun openUrl(url: String) {
    val uri = Uri.parse(url)
    try {
      val intent = Intent(Intent.ACTION_VIEW, uri)
      startActivity(intent)
      finish()
    } catch (ex: android.content.ActivityNotFoundException) {
      Toast.makeText(this@AboutActivity, "There is no web client installed.",
          Toast.LENGTH_SHORT).show()
    }

  }
}





