package v76archenko.dewill.simpletodo

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.AdapterView
import android.widget.EditText
import android.widget.ListView
import io.realm.Realm
import io.realm.RealmResults


import kotlinx.android.synthetic.main.activity_to_do.*
import v76archenko.dewill.simpletodo.model.Task
import v76archenko.dewill.simpletodo.utils.CredentialsManager
import java.util.*

class ToDoActivity : AppCompatActivity() {
  private lateinit var realm: Realm
  private lateinit var taskEditText: EditText
  private val TAG = "ToDoActivity"

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_to_do)
    setSupportActionBar(toolbar)
    initListView()

    fab.setOnClickListener {
      showAddDialog()
    }
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menuInflater.inflate(R.menu.menu_to_do, menu)
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      R.id.action_delete -> {
        deleteAllDone();
        Log.i(TAG, "delete all was pressed")
        true
      }
      R.id.action_about -> {
        startActivity(Intent(this@ToDoActivity, AboutActivity::class.java))
        true
      }
      R.id.action_log_out -> {
        logout()
        true
      }
      else -> super.onOptionsItemSelected(item)
    }
  }

  private fun initListView() {
    realm = Realm.getDefaultInstance()
    var taskResult: RealmResults<Task> = realm.where(Task::class.java).findAll()
    taskResult = taskResult.sort("timestamp")
    val adapter = TaskAdapterconstructor(this, taskResult)
    val listView = findViewById<ListView>(R.id.task_list)
    listView.adapter = adapter
    listView.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, i, _ ->
      val task: Task = adapterView.adapter.getItem(i) as Task
      taskEditText = EditText(this@ToDoActivity)
      taskEditText.setText(task.name)
      showEditDialog(task)
    }
  }


  override fun onDestroy() {
    super.onDestroy()
    realm.close()
  }

  fun changeTaskDone(taskId: String) {
    realm.executeTransactionAsync { realm ->
      val task: Task = realm.where(Task::class.java).equalTo("id", taskId).findFirst()!!
      task.done = !(task.done)
      Log.i("TAG", task.done.toString())
    }
  }

  private fun changeTaskName(taskId: String, name: String) {
    realm.executeTransactionAsync { realm ->
      val task: Task = realm.where(Task::class.java).equalTo("id", taskId).findFirst()!!
      task.name = name
    }
  }


  /////////delete from Realm without lambda
  private fun deleteTask(taskId: String) {
    realm.executeTransactionAsync(object : Realm.Transaction {
      override fun execute(realm: Realm?) {
        realm!!.where(Task::class.java).equalTo("id", taskId)
            .findFirst()!!
            .deleteFromRealm()
      }
    })
    Log.i(TAG, "Delete task successfully")
  }

  private fun deleteAllDone() {
    realm.executeTransactionAsync { realm: Realm? ->
      realm!!.where(Task::class.java)
          .equalTo("done", true)
          .findAll().deleteAllFromRealm()
    }
    Log.i(TAG, "AllDelete successfully")
  }

  private fun showAddDialog() {
    taskEditText = EditText(this@ToDoActivity)
    val alertDialog = AlertDialog.Builder(this@ToDoActivity)
        .setTitle("Add Task")
        .setView(taskEditText)
        .setPositiveButton("Add") { _, _ ->
          realm.executeTransactionAsync { realm ->
            val task = realm.createObject(Task::class.java, UUID.randomUUID().toString())
            task.name = taskEditText.text.toString()
            task.timestamp = System.currentTimeMillis()

            Log.i(TAG, "Add task : successfully ")
          }


        }.setNegativeButton("Cancel", null).create()
    alertDialog.show()
  }

  private fun showEditDialog(task: Task) {
    val dialog = AlertDialog.Builder(this@ToDoActivity)
        .setTitle("Edit Task")
        .setView(taskEditText)
        .setPositiveButton("Save") { _, _ ->
          changeTaskName(task.id, taskEditText.text.toString())
        }
        .setNegativeButton("Delete") { _, _ ->
          deleteTask(task.id)
        }.create()
    dialog.show()
  }


  private fun logout() {
    CredentialsManager.deleteCredentials(this@ToDoActivity)
    startActivity(Intent(this@ToDoActivity, LoginActivity::class.java))
  }
}
