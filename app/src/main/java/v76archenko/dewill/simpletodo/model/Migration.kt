package v76archenko.dewill.simpletodo.model

import io.realm.*

/**
 * Created by dewill on 19.02.2018.
 */
class Migration : RealmMigration {

  override fun migrate(realm: DynamicRealm?, oldVersion: Long, newVersion: Long) {
    var schemeVesion = oldVersion
    var schema = realm!!.schema
    if (oldVersion == 0L) {
      var taskScheme = schema.get("Task")
      taskScheme!!.addField("timestamp", Long::class.java).transform { obj ->
        obj.set("timestamp", 0)
      }
      schemeVesion++

    }
  }
}