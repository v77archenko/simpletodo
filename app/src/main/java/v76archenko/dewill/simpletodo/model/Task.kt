package v76archenko.dewill.simpletodo.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required

/**
 * Created by dewill on 15.02.2018.
 */
open class Task : RealmObject() {
  @PrimaryKey
  @Required
  var id: String = ""
  @Required
  var name: String = ""
  var done: Boolean = false
  var timestamp: Long = 0

}