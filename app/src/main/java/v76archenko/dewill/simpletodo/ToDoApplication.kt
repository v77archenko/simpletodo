package v76archenko.dewill.simpletodo

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration
import v76archenko.dewill.simpletodo.model.Migration

/**
 * Created by dewill on 15.02.2018.
 */
class ToDoApplication : Application() {

  override fun onCreate() {
    super.onCreate()
    initRealm()
  }


  private fun initRealm() {
    Realm.init(this)
    val config = RealmConfiguration.Builder()
        .name("todo.realm")
        .schemaVersion(1)
        .migration(Migration())
        .build()
    Realm.setDefaultConfiguration(config)

  }
}