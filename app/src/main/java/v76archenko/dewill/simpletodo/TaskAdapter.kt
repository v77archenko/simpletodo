package v76archenko.dewill.simpletodo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ListAdapter
import android.widget.TextView
import io.realm.OrderedRealmCollection
import io.realm.RealmBaseAdapter
import v76archenko.dewill.simpletodo.model.Task

/**
 * Created by dewill on 15.02.2018.
 */
class TaskAdapterconstructor(var activity: ToDoActivity,
    data: OrderedRealmCollection<Task>) : RealmBaseAdapter<Task>(data), ListAdapter {

  private companion object {
    class ViewHolder {
      lateinit var taskName: TextView
      lateinit var isTaskDone: CheckBox
    }
  }


  override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
    var convertView = convertView
    var viewHolder: ViewHolder
    if (convertView == null) {
      convertView = LayoutInflater.from(parent!!.context).inflate(R.layout.task_list_row, parent,
          false)
      viewHolder = ViewHolder()
      viewHolder.taskName = convertView!!.findViewById(R.id.task_item_name)
      viewHolder.isTaskDone = convertView!!.findViewById(R.id.task_item_done)
      viewHolder.isTaskDone.setOnClickListener(listener)
      convertView.tag = viewHolder
    } else {
      viewHolder = convertView.tag as ViewHolder
    }

    if (adapterData !== null) {
      var task: Task = adapterData!![position]
      viewHolder.taskName.text = task.name
      viewHolder.isTaskDone.isChecked = task.done
      viewHolder.isTaskDone.tag = position
    }
    return convertView
  }


  private var listener = View.OnClickListener({ view ->
    var position = view.tag as Int
    if (adapterData != null) {
      var task = adapterData!![position]
      activity.changeTaskDone(task.id)
    }
  })


}