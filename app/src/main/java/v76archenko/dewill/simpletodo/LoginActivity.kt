package v76archenko.dewill.simpletodo

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.auth0.android.Auth0
import com.auth0.android.authentication.AuthenticationAPIClient
import com.auth0.android.authentication.AuthenticationException
import com.auth0.android.callback.BaseCallback
import com.auth0.android.provider.AuthCallback
import com.auth0.android.provider.WebAuthProvider
import com.auth0.android.result.Credentials
import com.auth0.android.result.UserProfile
import v76archenko.dewill.simpletodo.utils.CredentialsManager

/**
 * Created by dewill on 20.02.2018.
 */
class LoginActivity : Activity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
  }

  override fun onDestroy() {
    super.onDestroy()
  }

  override fun onResume() {
    super.onResume()
    val account = Auth0(this)
    val accessToken = CredentialsManager.getCredentials(this@LoginActivity).accessToken
    if (accessToken == null) {
      WebAuthProvider.init(account)
          .withConnectionScope("openid", "offline_access")
          .start(this@LoginActivity, mCallback)
      return
    }
    var aClient = AuthenticationAPIClient(account)
    aClient.userInfo(accessToken)
        .start(object : BaseCallback<UserProfile, AuthenticationException> {
          override fun onFailure(error: AuthenticationException?) {
            showToastText("Session Expired, please Log In")
            CredentialsManager.deleteCredentials(this@LoginActivity)

            WebAuthProvider.init(account)
                .withConnectionScope("openid", "offline_access")
                .start(this@LoginActivity, mCallback)
          }

          override fun onSuccess(payload: UserProfile?) {
            showToastText("Automatic Login Success")
            startActivity(Intent(this@LoginActivity, ToDoActivity::class.java))
            finish()
          }
        })


  }

  private fun showToastText(text: String) {
    runOnUiThread { Toast.makeText(this@LoginActivity, text, Toast.LENGTH_SHORT).show(); }
  }

  private val mCallback = object : AuthCallback {
    override fun onSuccess(credentials: Credentials) {
      showToastText("Log In - Success")
      CredentialsManager.saveCredentials(this@LoginActivity, credentials)
      startActivity(Intent(this@LoginActivity, ToDoActivity::class.java))
      finish()
    }

    override fun onFailure(dialog: Dialog) {
      showToastText("Log in - Cancelled")
    }

    override fun onFailure(exception: AuthenticationException?) {
      showToastText("Log in - Error Occurred")
    }
  }


}