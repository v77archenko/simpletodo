package v76archenko.dewill.simpletodo.utils

import android.content.Context
import com.auth0.android.result.Credentials
import v76archenko.dewill.simpletodo.R

import v76archenko.dewill.simpletodo.utils.Constants.Companion.ACCESS_TOKEN
import v76archenko.dewill.simpletodo.utils.Constants.Companion.EXPRIRES_IN
import v76archenko.dewill.simpletodo.utils.Constants.Companion.ID_TOKEN
import v76archenko.dewill.simpletodo.utils.Constants.Companion.REFRESH_TOKEN
import v76archenko.dewill.simpletodo.utils.Constants.Companion.TOKEN_TYPE

/**
 * Created by dewill on 20.02.2018.
 */

class CredentialsManager {
  companion object {
    fun saveCredentials(context: Context, credentials: Credentials) {
      var sharedPref = context.getSharedPreferences(context.getString(R.string.auth0_preferences),
          Context.MODE_PRIVATE)
      sharedPref.edit()
          .putString(ID_TOKEN, credentials.idToken)
          .putString(REFRESH_TOKEN, credentials.refreshToken)
          .putString(ACCESS_TOKEN, credentials.accessToken)
          .putString(TOKEN_TYPE, credentials.type)
          .putLong(EXPRIRES_IN, credentials.expiresIn!!)
          .apply()
    }

    fun getCredentials(context: Context): Credentials {
      var sharedPref = context.getSharedPreferences(context.getString(R.string.auth0_preferences),
          Context.MODE_PRIVATE)
      return Credentials(
          sharedPref.getString(ID_TOKEN, null),
          sharedPref.getString(ACCESS_TOKEN, null),
          sharedPref.getString(TOKEN_TYPE, null),
          sharedPref.getString(REFRESH_TOKEN, null),
          sharedPref.getLong(EXPRIRES_IN, 0)
      )
    }

    fun deleteCredentials(context: Context) {
      var sharedPref = context.getSharedPreferences(context.getString(R.string.auth0_preferences),
          Context.MODE_PRIVATE)
      sharedPref.edit()
          .putString(ID_TOKEN, null)
          .putString(REFRESH_TOKEN, null)
          .putString(ACCESS_TOKEN, null)
          .putString(TOKEN_TYPE, null)
          .putLong(EXPRIRES_IN, 0)
          .apply()
    }
  }
}