package v76archenko.dewill.simpletodo.utils

/**
 * Created by dewill on 20.02.2018.
 */
class Constants {
  companion object {
    val REFRESH_TOKEN = "refresh_token"
    val ACCESS_TOKEN = "access_token"
    val ID_TOKEN = "id_token"
    val TOKEN_TYPE = "token_type"
    val EXPRIRES_IN = "expires_in"
  }
}